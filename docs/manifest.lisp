(:docstring-markup-format :scriba
 :systems (:gases)
 :documents ((:title "Gases"
              :authors ("Nick Matvyeyev")
              :output-format (:type :multi-html
                              :template :minima)
              :sources ("manual.scr"))))